# bs23_task_repo

# State management : Getx
# local database : Sqflite
# Network : Httpclient
# Dependency injection : Getx

# i didn't use binding and routes for some reason i don't like it (when reading code go to another file and read it's code is not good for me)


A class  database_helper.dart file is created to handle all the database related operation which is a singleton class.
A class session_manager.dart file is created to handle all the shared preference related operation.
A class api_provider.dart file is created to handle all the api related operation.

A folder global_widget is created to store all the global widget.
A folder model is created to store all the model class.
A folder global_controller is created to store all the global controller class.
A folder utils is created to store all utility class.

In the main.dart i have used a connectivity controller to check the internet connection for whole app life cycle

In the repository listview i have fetched ten new data every time from the api and stored it in the local database.
on scrolling i have added a listener when we get to the end of the listview it will fetch another ten data from the api and store it in the local database.
when we click on the item in the listview it will navigate to the detail page and show the detail of the item.

keep record of the last page number in the local database by shared preference
keep record of the sort order in the local database by shared preference
keep record of the total repository count in the local database by shared preference from api

# how to run the project
simply clone the project and run "flutter pub get" in the terminal
then run the project by "flutter run" in the terminal

thank you
```



