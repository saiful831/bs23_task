import 'package:bs23_task/global_controller/connectivity_controller.dart';
import 'package:bs23_task/pages/repository_list_page/repository_list_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future<void> main() async {
  Get.put(ConnectivityController());
 await  Future.delayed(const Duration(seconds: 1));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return const GetMaterialApp(
      title: 'Brain Station 23 Task',
      home:  RepositoryListPage(),
    );
  }
}


