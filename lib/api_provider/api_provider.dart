import 'dart:io';
import 'dart:convert' show json, utf8;

import 'package:bs23_task/database/database_helper.dart';
import 'package:bs23_task/model/repository.dart';
import 'package:bs23_task/session_manager/session_manager.dart';
import 'package:bs23_task/utils/constants.dart';

class ApiProvider {
  DatabaseHelper databaseHelper = DatabaseHelper.instance;
  SessionManager prefs = SessionManager();
  static final HttpClient _httpClient = HttpClient();

  Future<bool?> fetchRepositoryList({String? query,String? perPage,String? pageNumber,bool? deleteBeforeInsert}) async {
    final uri = Uri.parse(BASE_URl+endpoint_repositories_list).replace(
      queryParameters: {
        'q': query,
        'sort': 'stars',
        'order': 'desc',
        'per_page': perPage,
        'page': pageNumber,
      },
    );
    final jsonResponse = await _getJson(uri);

    if (jsonResponse == null) {
      return null;
    }
    if (jsonResponse['errors'] != null) {
      return null;
    }
    if (jsonResponse['items'] == null) {
      return null;
    }
   await  databaseHelper.insertList(deleteBeforeInsert: deleteBeforeInsert??false,tableName:DatabaseHelper.table_repository,dataList:Repository.mapJSONStringToList(jsonResponse['items']).map((e) => e.toJson()).toList());
   await prefs.setTotalRepository(jsonResponse['total_count']);
   return true;
  }

  static Future<Map<String, dynamic>?> _getJson(Uri uri) async {
    try {
      final httpRequest = await _httpClient.getUrl(uri);
      print(httpRequest.uri.toString());
      final httpResponse = await httpRequest.close();
      if (httpResponse.statusCode != HttpStatus.OK) {
        return null;
      }

      final responseBody = await httpResponse.transform(utf8.decoder).join();
      return json.decode(responseBody);
    } on Exception catch (e) {
      print('$e');
      return null;
    }
  }
}
