import 'dart:convert';

class Repository {
  final String? name;
  final String? description;
  final String? language;
  final int? stars;
  final int? forks;
  final DateTime? updatedAt;
  final Map<String,dynamic>? json;

  Repository({
    this.name,
    this.description,
    this.language,
    this.stars,
    this.forks,
    this.updatedAt,
    this.json,
  });

  factory Repository.fromJson(Map<String, dynamic> json) {
    return Repository(
      name: json['name'],
      description: json['description'],
      language: json['language'],
      stars: json['stargazers_count'],
      forks: json['forks_count'],
      updatedAt: DateTime.parse(json['updated_at']),
      json:  json['full_object'] == null ? json : jsonDecode(json['full_object']),
    );
  }
  Map<String,dynamic>toJson()=>{

      "name":name,
      "description":description,
      "language":language,
      "stargazers_count":stars,
      "forks_count":forks,
      "updated_at":updatedAt.toString(),
      "full_object": jsonEncode(json),
  };

  static List<Repository> mapJSONStringToList(List<dynamic> jsonList) {
    List<Repository> list = [];
    for (var json in jsonList) {
      json['full_object']=jsonEncode(json);
      list.add(Repository.fromJson(json));
    }
    return list;
  }
}