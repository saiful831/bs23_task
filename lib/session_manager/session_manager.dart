
import 'package:bs23_task/utils/enums.dart';
import 'package:nb_utils/nb_utils.dart';

import '../utils/constants.dart';


class SessionManager {
  //set pageNumber
  Future<void> setPageNumber(int pageNumber) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(PREFS_PAGENUMBER, pageNumber);
  }
  //get pageNumber
  Future<int> getPageNumber() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getInt(PREFS_PAGENUMBER) ?? 1;
  }

  //set lastSyncTime
  Future<void> setLastSyncTime(DateTime lastSyncTime) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(PREFS_LASTSYNCTIME, lastSyncTime.millisecondsSinceEpoch);
  }
  //get lastSyncTime
  Future<DateTime> getLastSyncTime() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return DateTime.fromMillisecondsSinceEpoch(pref.getInt(PREFS_LASTSYNCTIME) ?? 0);
  }

  //set sortBy
  Future<void> setSortBy(SortBy sortBy) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(PREFS_SORTBY, sortBy.index);
  }

  //get sortBy
  Future<SortBy> getSortBy() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return SortBy.values[pref.getInt(PREFS_SORTBY) ?? 0];
  }

  //set totalRepository
  Future<void> setTotalRepository(int totalRepository) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(PREFS_TOTALREPOSITORY, totalRepository);
  }

  //get totalRepository
  Future<int?> getTotalRepository() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getInt(PREFS_TOTALREPOSITORY);
  }


}
