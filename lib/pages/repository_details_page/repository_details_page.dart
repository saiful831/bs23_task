import 'package:bs23_task/global_widget/label_value.dart';
import 'package:bs23_task/model/repository.dart';
import 'package:bs23_task/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nb_utils/nb_utils.dart';

class RepositoryDetailsPage extends StatelessWidget {
  Repository repository;
  RepositoryDetailsPage({Key? key,required this.repository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Repository Details Page"),
      ),
      body: Column(
        children: [
          Text(repository.json!['name']??"",style: const TextStyle(fontSize: 22,fontWeight: FontWeight.w900),).paddingAll(8),
          Container(
             height: context.height() * 0.3,
              padding: const EdgeInsets.all(8),
              margin: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 1,
                ),
              ),
              child: Image.network(repository.json!['owner']['avatar_url']),
          ),

          Expanded(
            child: ListView(
              shrinkWrap: true,
              physics: const ScrollPhysics(),
                children: [
                  LabelValue(label: "Description", value: repository.json!['description'],borderColor: Colors.red,),
                  LabelValue(label: "Last Update", value: DateFormat(TwoDigitFormatedDate).format(repository.updatedAt!),borderColor: Colors.red,),
                  LabelValue(label: "Created At", value: DateFormat(TwoDigitFormatedDate).format(DateTime.parse(repository.json!['created_at']))),
                  LabelValue(label: "Language", value: repository.json!['language']),
                  LabelValue(label: "Stars", value: repository.json!['stargazers_count'].toString()),
                  LabelValue(label: "Forks", value: repository.json!['forks_count'].toString()),
                  LabelValue(label: "Watchers", value: repository.json!['watchers_count'].toString()),
                  LabelValue(label: "Open Issues", value: repository.json!['open_issues_count'].toString()),
                  LabelValue(label: "Subscribers", value: repository.json!['subscribers_count'].toString()),
                  LabelValue(label: "Default Branch", value: repository.json!['default_branch']),
                  LabelValue(label: "Size", value: repository.json!['size'].toString()),
                  LabelValue(label: "Forks", value: repository.json!['forks'].toString()),
                  LabelValue(label: "Open Issues", value: repository.json!['open_issues'].toString()),
                  ...(repository.json!['owner'] as Map<String, dynamic>).entries.map((entry) {
                    bool isURLValid = entry.value.runtimeType != String ? false : Uri.parse(entry.value).host.isNotEmpty;
                    return Container(
                      padding: const EdgeInsets.all(8),
                      child: LabelValue(
                          label: entry.key.toUpperCase().replaceAll('_', ' '),
                        // value:  Uri.parse(entry.value.webLink).isAbsolute.toString()??"",
                         value: entry.value.toString()??"",
                        valueColor: isURLValid?Colors.blue:null,
                      ),
                    );
                  }).toList(),

                ],
              ),
          ),



        ],
      ),
    );
  }
}
