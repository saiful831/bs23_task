import 'dart:async';
import 'package:bs23_task/api_provider/api_provider.dart';
import 'package:bs23_task/database/database_helper.dart';
import 'package:bs23_task/global_controller/connectivity_controller.dart';
import 'package:bs23_task/model/repository.dart';
import 'package:bs23_task/session_manager/session_manager.dart';
import 'package:bs23_task/utils/enums.dart';
import 'package:bs23_task/utils/utility_functions.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class RepositoryListController extends GetxController{
  DatabaseHelper databaseHelper = DatabaseHelper.instance;
  ScrollController scrollController = ScrollController();
  SessionManager prefs=SessionManager() ;
  var connectionStatus = Get.find<ConnectivityController>();

  late DateTime _lastRefreshTime;
  var canRefresh = true.obs; // Initialize with true
  late Timer timer;
  var remainingTime = 'Ready to refresh'.obs;
  var sortBy=SortBy.SortByStarsDescending.obs;


  var repositoryList=<Repository>[].obs;
  var pageNumber=1.obs;
  var isNoMoreData=false.obs;
  var isDataLoading=false.obs;


  @override
  Future<void> onInit() async {
    super.onInit();

    sortBy.value=await prefs.getSortBy();
    _initialize();
    scrollController.addListener(_onScroll);
    await getTenNewItem();
    fetchLocalData();


  }
  @override
  void onClose() {
    scrollController.dispose();
    timer.cancel();
    super.onClose();
  }

  Future<void> _onScroll() async {
    if (scrollController.position.atEdge && scrollController.position.pixels > 0) {



      if ( (await prefs.getTotalRepository()??-1)>repositoryList.length ){
        if(connectionStatus.isInternetConnected.value==true){
          print("i am workng");
          await getTenNewItem();
        }

        fetchLocalData();
      }else{
        isNoMoreData.value=true;
      }
      update(); // Trigger a re-build
      notifyChildrens();
      refresh();
    }
  }

  Future<void> getTenNewItem() async {
    if(pageNumber.value < await prefs.getPageNumber()){
       pageNumber.value++;
       return;
    }

    await ApiProvider().fetchRepositoryList(query: "Flutter",perPage: '10',pageNumber:pageNumber.value.toString() ).then((value) async {
      if (value!=null && value){
        pageNumber.value++;
        await prefs.setPageNumber(pageNumber.value);
      }
    });
  }

  Future<void> fetchLocalData() async {

    await databaseHelper.getAll(tbl:DatabaseHelper.table_repository,limit: 10,offset: repositoryList.length).then((value) {
      repositoryList.addAll(value.map((e) => Repository.fromJson(e)).toList());
      sortList(sortBy.value);
      update();
      notifyChildrens();
      repositoryList.refresh();
    });
  }

  Future<void> _initialize() async {
    _lastRefreshTime = await prefs.getLastSyncTime();
     canRefresh.value = DateTime.now().difference(_lastRefreshTime) > const Duration(minutes: 30);
    _startTimer();
  }

  void _startTimer() {
      timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (!canRefresh.value) {
        final remaining = _lastRefreshTime.add(const Duration(minutes: 30)).difference(DateTime.now());
        if (remaining.isNegative) {
          remainingTime.value = 'Ready to refresh';
          canRefresh.value = true;
          timer.cancel();
        } else {
          remainingTime.value = formatDuration(remaining);
        }
      }
    });
  }
  Future<void> refreshData() async {
    if (canRefresh.value) {
       await prefs.setLastSyncTime(DateTime.now());
      _lastRefreshTime = DateTime.now();
       canRefresh.value = false;
       prefs.setPageNumber(0);
       pageNumber.value=1;
       await databaseHelper.deleteAll(tableName: DatabaseHelper.table_repository);
       repositoryList.value=[];
       await getTenNewItem();
       fetchLocalData();
       update();
    }
  }


  sortList(SortBy sortBy) async {
   await prefs.setSortBy(sortBy);
    this.sortBy.value=sortBy;
    switch(sortBy){
      case SortBy.SortByStarsAscending:
        repositoryList.sort((a, b) => a.stars!.compareTo(b.stars!));
        break;
      case SortBy.SortByStarsDescending:
        repositoryList.sort((a, b) => b.stars!.compareTo(a.stars!));
        break;
        case SortBy.SortByLastUpdatedAscending:
        repositoryList.sort((a, b) => a.updatedAt!.compareTo(b.updatedAt!));
        break;
        case SortBy.SortByLastUpdatedDescending:
        repositoryList.sort((a, b) => b.updatedAt!.compareTo(a.updatedAt!));
        break;

    }
    update();
    notifyChildrens();
    repositoryList.refresh();
  }



}