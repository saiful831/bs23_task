import 'package:bs23_task/global_widget/global_button.dart';
import 'package:bs23_task/global_widget/repository_card_view.dart';
import 'package:bs23_task/pages/repository_details_page/repository_details_page.dart';
import 'package:bs23_task/pages/repository_list_page/repository_list_controller.dart';
import 'package:bs23_task/utils/enums.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nb_utils/nb_utils.dart';

class RepositoryListPage extends StatelessWidget {
  const RepositoryListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    RepositoryListController mvc = Get.put(RepositoryListController());
    return Scaffold(
      appBar: AppBar(
        title: const Text('Repository List Page'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            10.height,
                  Obx(() {

                    return SizedBox(
                      height: Get.height * 0.05,
                      child: GlobalButton(
                          onTap: mvc.connectionStatus.isInternetConnected.value
                              ? mvc.refreshData
                              : null,
                          text: 'Refresh Data',
                          isEnable: mvc.connectionStatus.isInternetConnected.value),
                    );
                  }),

            Obx(
                  () =>
                  Text(
                    mvc.connectionStatus.isInternetConnected.value
                        ? "Internet Connected"
                        : "Internet Disconnected",
                    style: TextStyle(
                        color: mvc.connectionStatus.isInternetConnected.value
                            ? Colors.green
                            : Colors.red),
                  ),
            ),
            const SizedBox(height: 10),
            Obx(
                  () =>
                  Text(
                    "You can refresh data in : ${mvc.remainingTime.value}",
                    style: TextStyle(
                        color: mvc.canRefresh.value ? Colors.green : Colors
                            .red),
                  ),
            ),
            10.height,
            Column(
              children: [
                Row(
                  children: [
                    //sort by star count ascending
                    Expanded(
                      child: Obx(
                            () =>
                            GlobalButton(
                              isEnable:
                              mvc.sortBy.value == SortBy.SortByStarsDescending,
                              onTap: () {
                                mvc.sortList(SortBy.SortByStarsDescending);
                              },
                              text: "Sort by star count descending",
                            ),
                      ),
                    ),
                    10.width,
                    Expanded(
                      child: Obx(
                            () =>
                            GlobalButton(
                              isEnable:
                              mvc.sortBy.value == SortBy.SortByStarsAscending,
                              onTap: () {
                                mvc.sortList(SortBy.SortByStarsAscending);
                              },
                              text: "Sort by star count ascending",
                            ),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    //sort by last updated date descending
                    Expanded(
                      child: Obx(
                            () =>
                            GlobalButton(
                              isEnable: mvc.sortBy.value ==
                                  SortBy.SortByLastUpdatedDescending,
                              onTap: () {
                                mvc.sortList(
                                    SortBy.SortByLastUpdatedDescending);
                              },
                              text: "Sort by last updated date descending",
                            ),
                      ),
                    ),
                    10.width,
                    Expanded(
                      child: Obx(() =>
                          GlobalButton(
                            isEnable: mvc.sortBy.value ==
                                SortBy.SortByLastUpdatedAscending,
                            onTap: () {
                              mvc.sortList(SortBy.SortByLastUpdatedAscending);
                            },
                            text: "Sort by last updated date ascending",
                          )),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(
              height: Get.height * 0.6,
              child: Obx(
                    () {
                  return ListView.builder(
                    shrinkWrap: true,
                    physics: const ScrollPhysics(),
                    controller: mvc.scrollController,
                    itemCount: mvc.repositoryList.length,
                    itemBuilder: (context, index) {
                      var repository = mvc.repositoryList[index];

                      return RepositoryCardView(
                          repository: repository,
                          onItemTap: () {
                            Get.to(
                                  () =>
                                  RepositoryDetailsPage(
                                    repository: repository,
                                  ),
                            );
                          },
                          index: index);
                    },
                  );
                },
              ),
            ),
            Obx(() =>
            Column(
              children: [
                 mvc.isNoMoreData.value
                    ? const Text('No more data',style: TextStyle(color: Colors.red,fontSize: 20),)
                    :  Container()

              ],
            )

            ),

          ],
        ),
      ),
    );
  }
}
