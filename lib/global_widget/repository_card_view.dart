
import 'package:bs23_task/global_widget/label_value.dart';
import 'package:bs23_task/model/repository.dart';
import 'package:bs23_task/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nb_utils/nb_utils.dart';

class RepositoryCardView extends StatelessWidget {
  VoidCallback onItemTap;
  Repository repository;
  int index;
  RepositoryCardView({Key? key,required this.repository,required this.onItemTap,required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
       onTap: onItemTap,
      child: Container(
        margin: const EdgeInsets.all(10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black),),

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("${index+1} ${repository.name ?? ""}",
              style: const TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
            5.height,
            LabelValue(label: "Stars",value: repository.stars?.toString() ?? "",borderColor: Colors.transparent,padding: EdgeInsets.zero,),
            LabelValue(label: "Date",value: DateFormat(FormatedDate).format(repository.updatedAt ?? DateTime.now()),borderColor: Colors.transparent,padding: EdgeInsets.zero,),
          ],

        ),
      ),
    );
  }
}
