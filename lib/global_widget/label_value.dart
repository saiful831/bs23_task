
import 'package:bs23_task/utils/const_style.dart';
import 'package:flutter/material.dart';

class LabelValue extends StatelessWidget {
  String label;
  String value;
  String? dividerText;
  double? labelFontSize=14;
  double? valueFontSize;
  int? labelFlex;
  int? valueFlex;
  TextAlign? valueTextAlign;
  TextAlign? labelTextAlign;
  EdgeInsets? margin;
  EdgeInsets? padding;

  int? labelFontWeight;
  int? valueFontWeight;
  Color? labelColor;
  Color? valueColor;
  Color? borderColor;
  double? borderWidth;

  LabelValue({Key? key,
    required this.label,
    required this.value,
    this.dividerText,
    this.labelFontSize,
    this.valueFontSize,
    this.labelFlex,
    this.valueFlex,
    this.labelFontWeight,
    this.valueFontWeight,
    this.labelColor,
    this.valueColor,

    this.labelTextAlign,
    this.valueTextAlign,
    this.margin,
    this.padding,
    this.borderColor,
    this.borderWidth,



  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return   Container(
      margin: margin?? const EdgeInsets.only(left: 5,right: 5,top: 5),
      padding: padding??const EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
      decoration: BoxDecoration(
        border: Border.all(
          color:borderColor ?? Colors.black,
          width: borderWidth ?? 1,
        ),
      ),
      child: Row(
        children: [
          Expanded(
            flex: labelFlex??1,
            child: Container(
              // width:85,
              child: Text(label,
                textAlign: labelTextAlign,
                style: TextStyle(
                    fontSize: labelFontSize??16,
                    fontWeight: defaultFontWeight[labelFontWeight??500],
                    color: labelColor ?? Colors.black
                ),

              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 5),
            child: Text(dividerText??":",
              style:TextStyle(fontSize: valueFontSize??16,
                  color: valueColor ?? Colors.black
              ),
            ),
          ),
          Expanded(
            flex: valueFlex??2,
            child: Container(
              margin: const EdgeInsets.only(left: 5),
              child: Text(value,
                textAlign: valueTextAlign,
                style: TextStyle(
                    fontSize: valueFontSize??15,
                    fontWeight: defaultFontWeight[valueFontWeight??500],
                    color: valueColor ?? Colors.black.withOpacity(0.6)

                ),


              ),
            ),
          )
        ],
      ),
    );
  }
}
