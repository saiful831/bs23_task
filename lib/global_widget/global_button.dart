
import 'package:flutter/material.dart';

class GlobalButton extends StatelessWidget {
  VoidCallback? onTap;
  String? text;
  bool isEnable;
  GlobalButton({Key? key,this.onTap,this.text,required this.isEnable}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed:  onTap,
      style: ElevatedButton.styleFrom(
        foregroundColor: Colors.white, backgroundColor: isEnable? Colors.green : Colors.grey,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
      child: Text(text??"Button"),
    );

  }
}
