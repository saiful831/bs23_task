import 'dart:async';

import 'package:bs23_task/pages/repository_list_page/repository_list_controller.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:get/get.dart';

class ConnectivityController extends GetxController {
  var isInternetConnected = false.obs;
  late StreamSubscription<ConnectivityResult> connectivitySubscription;

  @override
  void onInit() {
    super.onInit();
    checkInternetConnection();

    // Listen for connectivity changes
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      checkInternetConnection();
    });
  }

  Future<void> checkInternetConnection() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    isInternetConnected.value = (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi);

    if (Get.isRegistered<RepositoryListController>()) {
      var repositoryListController=Get.find<RepositoryListController>();
      if(repositoryListController.repositoryList.isEmpty) {
        await repositoryListController.getTenNewItem();
        await  repositoryListController.fetchLocalData();
      
      }
    }
    isInternetConnected.refresh();
    notifyChildrens();
    update();


  }

  @override
  void onClose() {
    // Cancel the stream subscription when the controller is closed
    connectivitySubscription.cancel();
    super.onClose();
  }
}